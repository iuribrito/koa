const route = require('koa-route');

module.exports = (app) => {
    app.use(route.get('/', e => {
        e.body = 'home'
    }));

    app.use(route.get('/page', e => {
        e.body = 'page'
    }));
}
