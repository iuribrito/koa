const Koa = require('koa')

const app = new Koa()
const router = require('./router.js')

router(app)

app.listen(3001, () => console.log('server started: http://localhost:3001'))